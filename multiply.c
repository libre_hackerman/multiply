/*
 * Copyright (C) 2020 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define PAIR_DEFAULT 1
#define PAIR_GREEN 2
#define PAIR_BLUE 3

#define WBORDER_DEF(win) wborder(win, 0, 0, 0, 0, 0, 0, 0, 0)

int is_number(char *str)
{
    int i, is_num = 1;

    i = str[0] == '-' ? 1 : 0;
    while (str[i] && is_num)
    {
        if (!isdigit(str[i]))
            is_num = 0;
        i++;
    }

    return (is_num);
}

void set_properties()
{
    /* Terminal size check */
    if (LINES < 17 || COLS < 25)
    {
        endwin();
        fprintf(stderr, "Terminal too small\n");
        exit(1);
    }

    /* Colors definitions */
    start_color();
    use_default_colors();
    init_pair(PAIR_DEFAULT, -1, -1);
    init_pair(PAIR_GREEN, COLOR_GREEN, -1);
    init_pair(PAIR_BLUE, COLOR_BLUE, -1);
    attrset(COLOR_PAIR(PAIR_DEFAULT));

}

void draw_table(WINDOW *win_table, int n)
{
    werase(win_table);
    WBORDER_DEF(win_table);

    /* Header */
    wattron(win_table, A_BOLD);
    mvwprintw(win_table, 1, 1, "Table of %d", n);
    wattroff(win_table, A_BOLD);
    /* Separator */
    mvwaddch(win_table, 2, 1, ACS_HLINE);
    for (int i = 0; i < 16; i++)
        waddch(win_table, ACS_HLINE);

    for (int i = 1; i <= 10; i++)
    {
        int color_pair = i % 2 == 0 ? PAIR_GREEN : PAIR_BLUE;

        wattron(win_table, COLOR_PAIR(color_pair));
        mvwprintw(win_table, i+2, 1, "%d x %d = %d", n, i, n*i);
        wattroff(win_table, COLOR_PAIR(color_pair));
    }
    wnoutrefresh(win_table);
}

void draw_edit(WINDOW *win_edit)
{
    werase(win_edit);
    WBORDER_DEF(win_edit);
    wnoutrefresh(win_edit);
}

void start()
{
    WINDOW *win_table, *win_edit;
    int keep_mainloop = 1;
    char input[5];

    set_properties();
    win_table = newwin(14, 19, 0, 1);
    win_edit = newwin(3, 7, 14, 8);

    mvaddstr(15, 1, "Input:         (q exits)");
    wnoutrefresh(stdscr);
    draw_table(win_table, 1);
    draw_edit(win_edit);
    doupdate();

    while (keep_mainloop)
    {
        wmove(win_edit, 1, 1);
        wgetnstr(win_edit, input, 4);
        if (strcmp(input, "q") == 0)
            keep_mainloop = 0;
        else
        {
            if (is_number(input))
                draw_table(win_table, atoi(input));
            draw_edit(win_edit);
            doupdate();
        }
    }

    delwin(win_table);
    delwin(win_edit);
}

int main()
{
    initscr();
    cbreak();

    start();

    endwin();
    return (0);
}
