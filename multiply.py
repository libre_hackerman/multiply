#!/usr/bin/env python3

# Copyright (C) 2020 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import curses


def is_number(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


class Multiply:
    def __init__(self):
        curses.wrapper(self.start)


    def set_properties(self, stdscr):
        if curses.LINES < 17 or curses.COLS < 25:
            exit(1)
        curses.echo()
        curses.use_default_colors()
        curses.init_pair(1, curses.COLOR_GREEN, -1)
        curses.init_pair(2, curses.COLOR_BLUE, -1)
        curses.init_pair(3, -1, -1)
        stdscr.attrset(curses.color_pair(3))
        self.win_table = curses.newwin(14, 19, 0, 1)
        self.win_edit = curses.newwin(3, 7, 14, 8)
        stdscr.addstr(15, 1, "Input:         (q exits)")
        stdscr.refresh()  # To draw "Input" label in stdscr


    def draw_table(self, n):
        self.win_table.erase()
        self.win_table.border()
        self.win_table.addstr(1, 1, "Table of {}".format(n), curses.A_BOLD)
        self.win_table.addstr(2, 1, "─"*17)
        for i in range(1, 11):
            self.win_table.addstr(i+2, 1, "{} x {} = {}".format(n, i, int(n)*i),
                         curses.color_pair((i % 2) + 1))
        self.win_table.refresh()


    def draw_edit(self):
        self.win_edit.erase()
        self.win_edit.border()
        self.win_edit.refresh()


    def start(self, stdscr):
        self.set_properties(stdscr)
        self.draw_table(1)  # Default draw table of 1
        self.draw_edit()
        while True:
            self.win_edit.move(1, 1)
            inpt = self.win_edit.getstr(4).decode("utf-8")
            self.draw_edit()  # Clears win_edit
            if inpt == "q":
                break
            elif is_number(inpt):
                self.draw_table(inpt)


if __name__ == "__main__":
    Multiply()
